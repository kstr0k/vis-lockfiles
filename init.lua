-- Copyright (c) 2024 Florian Fischer. All rights reserved.
--
-- vis-lockfiles is free software: you can redistribute it and/or modify it under
-- the terms of the GNU General Public License as published by the Free Software
-- Foundation, either version 3 of the License, or (at your option) any later
-- version.
--
-- vis-lockfiles is distributed in the hope that it will be useful, but WITHOUT
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
-- FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License along with
-- vis-lockfiles found in the LICENSE file.
-- If not, see <https://www.gnu.org/licenses/>.
local source_str = debug.getinfo(1, 'S').source:sub(2)
local script_path = source_str:match('(.*/)')

local lockfiles = dofile(script_path .. 'lockfiles.lua')

local M = {}
M.directory = '${XDG_STATE_HOME:-${HOME}/.local/state}/vis/locks'
M.directory_expanded = nil

-- Detect available backends
if os.execute('command -v whiptail') then
  M.prompt_backend = 'whiptail'
else
  M.prompt_backend = 'vis-menu'
end

local function get_lock_dir()
  if M.directory_expanded ~= M.directory then
    M.directory = lockfiles.capture_cmd('printf %s "' .. M.directory .. '"')
    M.directory_expanded = M.directory
  end
  return M.directory
end

local function prompt_backend_whiptail(msg, menu)
  local cmd = 'whiptail </dev/tty 2>&1 >/dev/tty --title "vis-lockfiles" --nocancel'
  return vis:pipe(cmd .. ' --menu \'' .. msg .. '\n\' 0 0 0 ' .. menu, true)
end

local function prompt_backend_vis_menu(msg, menu, choices)
  local cmd1 = 'printf >/dev/tty %s \'' .. msg .. '\n\''
  local cmd2 = 'printf %s \'' .. choices .. '\''
  local cmd3 = 'vis-menu -b -p \'' .. menu .. '\''
  return vis:pipe(cmd1 .. ';' .. cmd2 .. '|' .. cmd3, true)
end

local function prompt_backend(path, lfile_path, pid, backend)
  local msg = 'File is locked: ' .. path .. '\n  lock: ' .. lfile_path ..
                  '\n  owned by process ID: ' .. pid
  local status, output

  if backend == 'whiptail' then
    local menu = 'abort "operation (a)" edit "anyway (e)" quit "vis (q)"'
    status, output = prompt_backend_whiptail(msg, menu)
  elseif type(backend) == 'function' then
    status, output = backend(path, lfile_path, pid)
  else
    status, output = prompt_backend_vis_menu(msg, '(E)dit anyway, (Q)uit, [A]bort', 'a\ne\nq')
  end
  vis:redraw()

  return (status == 0) and output:sub(1, 1)
end

-- get a user confirmation
-- return true if user wants to edit the locked file
local function prompt(path, lfile_path, pid)

  local output = prompt_backend(path, lfile_path, pid, M.backend)
  if output == 'e' then
    return true
  elseif output == 'q' then
    vis:exit(1)
  end

  -- else 'abort' or dialog canceled
  vis:command('q')
  vis:command('new')
end

local created_locks = {}

vis.events.subscribe(vis.events.WIN_OPEN, function(win)
  local file = win.file
  if created_locks[file.path] or not file.path then
    -- either a noname buffer, or already locked by us
    return
  end

  local lfile_path = lockfiles.lock_file_path(get_lock_dir(), file.path)
  local lock_pid = lockfiles.create_lock_file(get_lock_dir(), lfile_path)
  if lockfiles.vis_pid == lock_pid then
    created_locks[file.path] = lfile_path
  else
    if not prompt(file.path, lfile_path, lock_pid) then
      -- return ~= nil to stop further event handling since we closed the window
      return true
    end
  end
end)

vis.events.subscribe(vis.events.FILE_CLOSE, function(file)
  if not file.path then
    return
  end
  local lfile_path = created_locks[file.path]
  if lfile_path then
    lockfiles.delete_lock_file(lfile_path)
    created_locks[file.path] = nil
  end
end)

vis.events.subscribe(vis.events.QUIT, function()
  for _, lfile_path in pairs(created_locks) do
    if lfile_path then
      lockfiles.delete_lock_file(lfile_path)
    end
  end
  created_locks = {}
end)

return M
