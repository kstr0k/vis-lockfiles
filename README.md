# vis-lockfiles - detect concurrent edits

This is a simple `vis` plugin to detect if a file is concurrently edited by
multiple vis processes.

## Configuration

By default, the lockfiles will be placed in `${XDG_STATE_HOME:-${HOME}/.local/state}/vis/locks/`.

* The `directory` member of the module table can be used to define the lock file location.
* The `backend` member can be used to specify one of the supported backends: `whiptail` or `vis-menu`.
  Additionally, it can provide a custom backend function with the following signature `f(filename, lockfilename, lockpid) => bool, ['a', 'e', 'q']`.


```lua
local lockfiles = require('plugins/vis-lockfiles')
lockfiles.direcory = '$HOME'
```
